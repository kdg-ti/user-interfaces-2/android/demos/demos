package be.kdg.fragmentdemo2021

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.core.os.bundleOf
import androidx.fragment.app.commit

class MainActivity : AppCompatActivity(), BlankFragment.Fragment1Listener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<Button>(R.id.btnSecond).setOnClickListener {
            showSecondFragment()
        }
    }

    private fun showSecondFragment(name:String = "no name"){
        val bundle = bundleOf("name" to name)
        supportFragmentManager.commit {
            setReorderingAllowed(true)
            replace(R.id.fragment_container, BlankFragment2::class.java, bundle)
            addToBackStack(null)
        }
    }

    override fun nameEntered(name: String) {
        showSecondFragment(name)
    }
}