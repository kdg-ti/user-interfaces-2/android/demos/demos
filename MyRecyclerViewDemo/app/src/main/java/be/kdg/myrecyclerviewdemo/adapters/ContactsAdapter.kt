package be.kdg.myrecyclerviewdemo.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import be.kdg.myrecyclerviewdemo.MainActivity
import be.kdg.myrecyclerviewdemo.R

class ContactsAdapter(val listener: ContactsListener): RecyclerView.Adapter<ContactsAdapter.ContactViewholder>() {
    class ContactViewholder(itemView: View): RecyclerView.ViewHolder(itemView){
        val textView = itemView.findViewById<TextView>(R.id.tvName)
        val btnRemove = itemView.findViewById<Button>(R.id.btnRemove)
    }
    val contacts = arrayListOf("jos","jef","dirk","frank", "jos","jef","dirk","frank","jos","jef","dirk","frank")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewholder {
        Log.d("mylogs", "viewholder created...")
        val contactView = LayoutInflater.from(parent.context).inflate(R.layout.contacts_item, parent, false)
        return ContactViewholder(contactView)
    }

    override fun onBindViewHolder(holder: ContactViewholder, position: Int) {
        Log.d("mylogs", "viewholder recycled...")
        holder.itemView.setOnClickListener {
            listener.contactSelected(position)
        }
        holder.textView.text = contacts[position]
        holder.btnRemove.setOnClickListener{
            contacts.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    override fun getItemCount() = contacts.size

    interface ContactsListener{
        fun contactSelected(index: Int)
    }
}