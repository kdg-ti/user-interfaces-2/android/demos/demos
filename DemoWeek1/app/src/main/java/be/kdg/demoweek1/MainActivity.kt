package be.kdg.demoweek1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import java.util.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btnKlik = findViewById<Button>(R.id.btnKlik)
        val ivDie = findViewById<ImageView>(R.id.ivDie)
        btnKlik.setOnClickListener {
            Toast.makeText(this, "Hallokes", Toast.LENGTH_LONG).show()
            val randomInt = Random().nextInt(6) + 1
            ivDie.setImageResource(
                resources.getIdentifier(
                    "die" + randomInt,
                    "drawable",
                    packageName
                )
            )
        }
    }
}