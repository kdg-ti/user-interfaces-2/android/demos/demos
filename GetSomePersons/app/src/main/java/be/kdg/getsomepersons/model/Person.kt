package be.kdg.getsomepersons.model

import com.google.gson.annotations.SerializedName

data class Person(
    val id: Int,

    // Gebruik '@SerializedName' indien je je property niet exact dezelfde naam wil geven
    // als het JSON veld. Dit kan handig zijn als je bvb. snake case gebruikt hebt in je JSON bestand.
    @SerializedName("firstName")
    val givenName: String,

    val married: Boolean
)
