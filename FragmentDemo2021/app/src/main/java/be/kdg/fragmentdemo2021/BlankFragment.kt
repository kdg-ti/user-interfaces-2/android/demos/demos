package be.kdg.fragmentdemo2021

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment


class BlankFragment : Fragment() {
    private lateinit var fragment1Listener: Fragment1Listener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Fragment1Listener) {
            this.fragment1Listener = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_blank, container, false)
        view.findViewById<Button>(R.id.btnNext).setOnClickListener {
            fragment1Listener.nameEntered(view.findViewById<EditText>(R.id.etName).text.toString())
        }
        return view
    }

    interface Fragment1Listener {
        fun nameEntered(name:String)
    }
}